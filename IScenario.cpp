//
// IScenario.cpp
//
//  Created on: 2019. m�rc. 13.
//      Author: fasz
//

#include <IScenario.hpp>

#include <ISpecimen.hpp>

ISpecimen* IScenario::createSpecimen(::std::istream& in) const
{
  ::std::mt19937 dummyRandom;
  ISpecimen* specimen(createSpecimen(dummyRandom));
  specimen->load(in);
  return specimen;
}
