//
// ISpecimen.hpp
//
//  Created on: 2019. m�rc. 10.
//      Author: fasz
//

#ifndef HF6BF7C19_E88E_410C_9856_57D33AD6EA1B
#define HF6BF7C19_E88E_410C_9856_57D33AD6EA1B

#include <iosfwd>
#include <random>

class ISpecimen
{
public:
  virtual ~ISpecimen()
  {
  }
  virtual void mutate(::std::mt19937& randomEngine) = 0;

  virtual void save(::std::ostream& out) const = 0;
  virtual void load(::std::istream& in) = 0;

  virtual ISpecimen* clone() const = 0;
};

#endif // HF6BF7C19_E88E_410C_9856_57D33AD6EA1B
