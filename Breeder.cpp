//
// Breeder.cpp
//
//  Created on: 2019. m�rc. 9.
//      Author: fasz
//

#include <Breeder.hpp>

#include <IScenario.hpp>
#include <ISpecimen.hpp>
#include <ScenarioFactory.hpp>

#include <windows.h>

#include <sstream>
#include <fstream>
#include <chrono>
#include <cstdlib>
#include <algorithm>

namespace
{
const char* RANDOM_FILE_NAME = "random";
const char* SPECIMENS_DIRECTORY_NAME = "specimens";
} // namespace

Breeder::Breeder(const ScenarioFactory& scenarioFactory): mScenarioFactory(scenarioFactory)
{
}

Breeder::~Breeder()
{
}

Breeder::Failure
Breeder::createProject(::std::string name, int population, ::std::string scenarioId, ::std::string scenarioSettings)
{
  mCurrentScenario.reset();
  if (CreateDirectory(name.c_str(), NULL))
  {
    mProjectName = name;
    mCurrentStep = 0;
    ::std::ofstream descriptor((name + "\\descriptor").c_str());
    if (!descriptor.is_open())
    {
      return Failure::FILE;
    }
    ::std::mt19937::result_type seed(::std::chrono::system_clock::now().time_since_epoch().count());
    descriptor << seed << ' ' << scenarioId << ' ' << scenarioSettings << '\n';
    mRandomEngine.seed(seed);
    mCurrentScenario.reset(mScenarioFactory.createScenario(scenarioId, scenarioSettings));
    if (nullptr == mCurrentScenario)
    {
      return Failure::SCENARIO;
    }
    mCurrentBreed.clear();
    mCurrentBreed.reserve(population);
    for (int i(0); i < population; ++i)
    {
      mCurrentBreed.push_back(SpecimenPtr(mCurrentScenario->createSpecimen(mRandomEngine)));
    }
    return saveStep(mCurrentStep) ? Failure::NONE : Failure::FILE;
  }
  else
  {
    return Failure::FILE;
  }
}

Breeder::Failure Breeder::loadProject(::std::string name)
{
  mCurrentScenario.reset();
  mProjectName = name;
  ::std::ifstream descriptor((name + "\\descriptor").c_str());
  if (!descriptor.is_open())
  {
    return Failure::FILE;
  }

  ::std::string scenarioId;
  {
    ::std::mt19937::result_type seed;
    if (!(descriptor >> seed >> scenarioId))
    {
      return Failure::FILE;
    }
  }

  ::std::string settings;
  ::std::getline(descriptor, settings);
  mCurrentScenario.reset(mScenarioFactory.createScenario(scenarioId, settings));
  if (nullptr == mCurrentScenario)
  {
    return Failure::SCENARIO;
  }

  // find the current step
  {
    WIN32_FIND_DATA findFileData;
    HANDLE findHandle = INVALID_HANDLE_VALUE;

    findHandle = FindFirstFile((LPCSTR)(name + "\\*").c_str(), &findFileData);
    if (findHandle == INVALID_HANDLE_VALUE)
    {
      return Failure::FILE;
    }
    int currentMax(-1);
    do
    {
      if (0 != (findFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
      {
        const int stepNumber(::std::atoi(findFileData.cFileName));
        if (stepNumber > currentMax)
        {
          currentMax = stepNumber;
        }
      }
    } while (FindNextFile(findHandle, &findFileData) != 0);
    if (-1 == currentMax)
    {
      return Failure::FILE;
    }
    mCurrentStep = currentMax;
  }

  return loadStep(mCurrentStep) ? Failure::NONE : Failure::FILE;
}

namespace
{
struct SortingPair
{
  unsigned specimenIndex;
  IScenario::Score score;
};
} // namespace

Breeder::Failure Breeder::breed()
{
  ::std::ofstream breedingLogs((generateStepPath(mCurrentStep) + "\\breedinglogs").c_str());
  ::std::ofstream matchLogs((generateStepPath(mCurrentStep) + "\\scoring").c_str());

  breedingLogs << "breeding " << mCurrentBreed.size() << " specimen\n";

  ::std::vector<ISpecimen*> scoredBreed;
  ::std::transform(
    mCurrentBreed.begin(), mCurrentBreed.end(), ::std::back_inserter(scoredBreed), [](const SpecimenPtr& uPtr) {
      return uPtr.get();
    });
  ::std::vector<IScenario::Score> scores(mCurrentScenario->scoreBreed(scoredBreed, mRandomEngine, matchLogs));
  ::std::vector<SortingPair> sortingVector;
  sortingVector.reserve(scores.size());
  breedingLogs << "final scoring:\n";
  for (unsigned i(0); i < scores.size(); ++i)
  {
    breedingLogs << "  " << i << ": " << scores[i] << '\n';
    sortingVector.push_back({i, scores[i]});
  }
  ::std::sort(begin(sortingVector), end(sortingVector), [](const SortingPair& lhs, const SortingPair& rhs) {
    return lhs.score > rhs.score;
  });

  ::std::vector<ISpecimen*> newBreed;
  newBreed.reserve(mCurrentBreed.size());
  unsigned numberToSelect(mCurrentBreed.size() / 2);
  for (unsigned i(0); i < mCurrentBreed.size(); ++i)
  {
    const SortingPair& pairToBreed(sortingVector[i % numberToSelect]);
    breedingLogs << "breeding " << pairToBreed.specimenIndex << " to " << newBreed.size() << '\n';
    newBreed.push_back(mCurrentBreed[pairToBreed.specimenIndex]->clone());
    //#ifdef PROGRESSIVE_MUTATION
    //#endif
    for (unsigned j(0); j < i / numberToSelect; ++j)
    {
      newBreed.back()->mutate(mRandomEngine);
    }
  }
  mCurrentBreed.clear();
  ::std::transform(newBreed.begin(), newBreed.end(), ::std::back_inserter(mCurrentBreed), [](ISpecimen* ptr) {
    return SpecimenPtr(ptr);
  });

  return saveStep(++mCurrentStep) ? Failure::NONE : Failure::FILE;
}

bool Breeder::saveStep(int step)
{
  ::std::string stepPath(generateStepPath(step));
  if (!CreateDirectory(stepPath.c_str(), NULL) && ERROR_ALREADY_EXISTS != GetLastError())
  {
    return false;
  }
  {
    ::std::ofstream random(stepPath + "\\" + RANDOM_FILE_NAME);
    random << mRandomEngine;
  }
  ::std::string basePath(stepPath + "\\" + SPECIMENS_DIRECTORY_NAME);
  if (!CreateDirectory(basePath.c_str(), NULL) && ERROR_ALREADY_EXISTS != GetLastError())
  {
    return false;
  }

  for (unsigned i(0); i < mCurrentBreed.size(); ++i)
  {
    ::std::ostringstream fileNameAssembler;
    fileNameAssembler.width(SpecimenFileNameLength);
    fileNameAssembler.fill('0');
    fileNameAssembler << i;

    ::std::ofstream file((basePath + "\\" + fileNameAssembler.str()).c_str());
    if (!file.is_open())
    {
      return false;
    }
    mCurrentBreed[i]->save(file);
  }
  return true;
}

bool Breeder::loadStep(int step)
{
  mCurrentBreed.clear();
  WIN32_FIND_DATA findFileData;
  HANDLE findHandle = INVALID_HANDLE_VALUE;

  {
    ::std::ifstream random(generateStepPath(step) + "\\" + RANDOM_FILE_NAME);
    random >> mRandomEngine;
  }

  ::std::string path(generateStepPath(step) + "\\" + SPECIMENS_DIRECTORY_NAME);

  findHandle = FindFirstFile((LPCSTR)((path + "\\*").c_str()), &findFileData);
  if (findHandle == INVALID_HANDLE_VALUE)
  {
    return false;
  }
  do
  {
    if (0 == (findFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
    {
      ::std::ifstream specimenFile((path + "\\" + findFileData.cFileName).c_str());
      if (!specimenFile.is_open())
      {
        return false;
      }
      ISpecimen* specimen(mCurrentScenario->createSpecimen(specimenFile));
      mCurrentBreed.push_back(::std::move(SpecimenPtr(specimen)));
    }
  } while (FindNextFile(findHandle, &findFileData) != 0);

  return true;
}

::std::string Breeder::generateStepPath(int step) const
{
  ::std::ostringstream stepPathAssembler;
  stepPathAssembler << mProjectName << '\\';
  stepPathAssembler.width(StepDirectoryNameLength);
  stepPathAssembler.fill('0');
  stepPathAssembler << step;
  return stepPathAssembler.str();
}
