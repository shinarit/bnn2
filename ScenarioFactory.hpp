//
// ScenarioFactory.hpp
//
//  Created on: 2019. m�rc. 10.
//      Author: fasz
//

#ifndef H4E9BF072_9A35_41A0_92AA_3A96EE8B8A88
#define H4E9BF072_9A35_41A0_92AA_3A96EE8B8A88

#include <string>

class IScenario;

class ScenarioFactory
{
public:
  IScenario* createScenario(::std::string id, ::std::string settings) const;
  ::std::string getHelpText(::std::string id) const;
};

#endif // H4E9BF072_9A35_41A0_92AA_3A96EE8B8A88
