//
// OneVsOneParallelRunner.cpp
//
//  Created on: 2019. m�rc. 29.
//      Author: fasz
//

#include <OneVsOneParallelRunner.hpp>

#include <ISpecimen.hpp>

#include <thread>
#include <mutex>

namespace
{
const unsigned DefaultWorkerCount = 4;

//[) intervals
struct Runner
{
  struct MatchDescriptor
  {
    unsigned p1;
    unsigned p2;
  };
  typedef ::std::vector<MatchDescriptor> Work;
  typedef ::std::function<Work()> WorkProducer;
  Runner(WorkProducer workProducer, const ::std::vector<ISpecimen*>& breed)
    : workProducer(workProducer), breed(breed), scores(breed.size(), IScenario::Score())
  {
  }

  void runMatches(const ::std::vector<ISpecimen*>& breed, OneVsOneParallelRunner::Processor processor)
  {
    Work work(workProducer());
    while (!work.empty())
    {
      for (const MatchDescriptor match : work)
      {
        ::std::unique_ptr<ISpecimen> p1(breed[match.p1]->clone());
        ::std::unique_ptr<ISpecimen> p2(breed[match.p2]->clone());
        OneVsOneParallelRunner::Score score(processor(p1.get(), p2.get()));
        scores[match.p1] += score.p1Score;
        scores[match.p2] += score.p2Score;
      }
      work = workProducer();
    }
  }

  const WorkProducer workProducer;
  const ::std::vector<ISpecimen*>& breed;
  ::std::vector<IScenario::Score> scores;
};

const unsigned WorkPacketSize = 100;

struct MatchGenerator
{
  MatchGenerator(unsigned breedSize, unsigned workSize): x(0), y(1), breedSize(breedSize), workSize(workSize)
  {
  }
  Runner::Work produceMatches()
  {
    Runner::Work result;
    result.reserve(workSize);
    ::std::unique_lock lock(productionMutex);
    for (; x < breedSize - 1; ++x)
    {
      for (; y < breedSize; ++y)
      {
        result.push_back({x, y});
        if (result.size() == workSize)
        {
          ++y;
          return result;
        }
      }
      y = x + 2;
    }
    return result;
  }

  unsigned x;
  unsigned y;
  ::std::mutex productionMutex;
  const unsigned breedSize;
  const unsigned workSize;
};
} // namespace

OneVsOneParallelRunner::OneVsOneParallelRunner()
{
}

::std::vector<IScenario::Score> OneVsOneParallelRunner::run(const ::std::vector<ISpecimen*>& breed, Processor processor)
{
  unsigned idealWorkerCount(::std::thread::hardware_concurrency());
  if (0 == idealWorkerCount)
  {
    idealWorkerCount = DefaultWorkerCount;
  }
  ::std::vector<Runner> runners;
  ::std::vector<::std::thread> workerThreads;
  runners.reserve(idealWorkerCount);
  workerThreads.reserve(idealWorkerCount);
  MatchGenerator generator(breed.size(), WorkPacketSize);
  for (unsigned i(0); i < idealWorkerCount; ++i)
  {
    runners.emplace_back(Runner(::std::bind(MatchGenerator::produceMatches, &generator), breed));
    workerThreads.emplace_back(
      ::std::thread(Runner::runMatches, &runners.back(), ::std::reference_wrapper(breed), processor));
  }

  for (auto& thread : workerThreads)
  {
    thread.join();
  }

  ::std::vector<IScenario::Score> result(breed.size(), IScenario::Score());
  for (unsigned i(0); i < idealWorkerCount; ++i)
  {
    for (unsigned j(0); j < runners[i].scores.size(); ++j)
    {
      result[j] += runners[i].scores[j];
    }
  }
  return result;
}
