//
// Breeder.hpp
//
//  Created on: 2019. m�rc. 9.
//      Author: fasz
//

#ifndef _BREEDER_HPP_
#define _BREEDER_HPP_

#include <vector>
#include <random>
#include <string>
#include <memory>

class ScenarioFactory;
class IScenario;
class ISpecimen;

//
//  project:
//  projectname/
//    descriptor
//      <random seed> <scenarioId> <scenario specific data>
//    00000000/
//      specimen/
//        0000
//          <specimen data>
//        0001
//        ...
//        N
//      <scoring data>
//      random
//        <random state at the start>
//    00000001/
//    ...
//    N/
//

class Breeder
{
public:
  Breeder(const ScenarioFactory& scenarioFactory);
  ~Breeder();

  enum class Failure
  {
    FILE,
    SCENARIO,
    NONE
  };
  Failure createProject(::std::string name, int population, ::std::string scenarioId, ::std::string scenarioSettings);
  Failure loadProject(::std::string name);
  Failure breed();

private:
  static const int SpecimenFileNameLength = 4;
  static const int StepDirectoryNameLength = 8;

  bool saveStep(int step);
  bool loadStep(int step);

  ::std::string generateStepPath(int step) const;

  ::std::mt19937 mRandomEngine;

  ::std::string mProjectName;
  typedef ::std::unique_ptr<ISpecimen> SpecimenPtr;
  ::std::vector<SpecimenPtr> mCurrentBreed;
  const ScenarioFactory& mScenarioFactory;
  ::std::unique_ptr<IScenario> mCurrentScenario;

  int mCurrentStep;
};

#endif //_BREEDER_HPP_
