//
// GenericRecurrentNetwork.hpp
//
//  Created on: 2019. m�rc. 13.
//      Author: fasz
//

#ifndef H8C483969_23DA_46DE_B3C1_D3A85B5FE1C9
#define H8C483969_23DA_46DE_B3C1_D3A85B5FE1C9

#include <ISpecimen.hpp>

#include <flatset.hpp>

#include <iosfwd>
#include <random>

class GenericRecurrentNetwork : public ISpecimen
{
public:
  GenericRecurrentNetwork(unsigned inputNodes, unsigned hiddenNodes, unsigned outputNodes);
  GenericRecurrentNetwork(::std::istream& input);

  unsigned inputNumber() const
  {
    return mInputNo;
  }
  unsigned outputNumber() const
  {
    return mOutputNo;
  }

  void step(const ::std::vector<int>& input);
  int getOutput(int index);

  virtual void mutate(::std::mt19937& mt) override;
  virtual void save(::std::ostream& output) const override;
  virtual void load(::std::istream& input) override;
  virtual GenericRecurrentNetwork* clone() const override;

  void resetMemory();

private:
  static const double EdgeMutationChance;
  static const double ConnectivityMutationChance;
  static const double NormalDistribution;
  //
  // weight is [-256 .. 256]
  //
  struct Node
  {
    int value = 0;
    int visited = 0;
    int applyWeight(int weight) const
    {
      return (value * weight) >> 8;
    }
  };
  struct Edge
  {
    int index;
    int weight;
  };
  friend bool operator<(const Edge& lhs, const Edge& rhs);

  typedef ::flat::flat_set<Edge> EdgeCollection;

  friend ::std::ostream& operator<<(::std::ostream& out, const EdgeCollection& edges);
  friend ::std::istream& operator>>(::std::istream& in, EdgeCollection& edges);

  bool outputNode(unsigned index) const
  {
    return index >= mInputNo + mHiddenNo;
  }
  int targetNodeNumber() const
  {
    return mHiddenNo + mOutputNo;
  }

  unsigned mInputNo;
  unsigned mHiddenNo;
  unsigned mOutputNo;

  ::std::vector<Node> mNodes;
  ::std::vector<EdgeCollection> mEdges;
};

#endif // H8C483969_23DA_46DE_B3C1_D3A85B5FE1C9
