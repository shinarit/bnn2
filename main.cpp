//
// main.cpp
//
//  Created on: 2019. m�rc. 9.
//      Author: fasz
//

#include <Breeder.hpp>
#include <ScenarioFactory.hpp>

#include <iostream>
#include <map>
#include <sstream>
#include <string>
#include <thread>
//#include "thread.hpp" //threading not supported in current MinGW version
#include <atomic>
#include <limits>

namespace
{
const char DefaultPrompt[] = "> ";

::std::atomic<bool> BreedingInProgress = false;
::std::atomic<bool> StopBreeding = false;

void runBreeding(Breeder& breeder, int steps)
{
  if (0 == steps)
  {
    steps = ::std::numeric_limits<int>::max();
  }
  ::std::cout << "start breeding for " << steps << " steps\n";
  int stepNo(0);
  for (; !StopBreeding && stepNo < steps; ++stepNo)
  {
    if (Breeder::Failure::NONE != breeder.breed())
    {
      ::std::cout << stepNo << ". breeding step FAILED\n";
      break;
    }
    else
    {
      ::std::cout << stepNo << ". breeding step complete\n";
    }
  }
  ::std::cout << "stopped breeding after " << stepNo << " steps\n";
  StopBreeding = false;
  BreedingInProgress = false;
}
} // namespace

int main(int argc, char* argv[])
{
  const ::std::map<char, ::std::string> commandDescriptions
    = {{{'n', "new project:\n\tn <projectName> <population> <scenario ID> <scenario settings>*"},
      {'l', "load project:\n\tl <projectName>"},
      {'b', "breeding:\n\tb [number of steps to execute, unlimited if omitted]"},
      {'s', "stop the breeding after the current one is finished:\n\ts"},
      {'i', "print information:\n\ti"},
      {'x', "exit breeder:\n\tx"},
      {'?', "print this help:\n\t?"},
      {'c', "print config of scenario:\n\tc <scenario ID>"}}};
  ScenarioFactory scenarioFactory;
  Breeder breeder(scenarioFactory);
  ::std::string prompt(DefaultPrompt);
  bool exit(false);
  bool interactive(1 == argc);
  while ((interactive && !exit) || 1 < argc)
  {
    ::std::cout << prompt;
    ::std::string line;
    if (argc > 1)
    {
      line = argv[1];
      ++argv;
      --argc;
    }
    else
    {
      ::std::getline(::std::cin, line);
    }
    if (!line.empty())
    {
      bool needDescription(false);
      bool needBreedingInProgressMessage(false);
      switch (line[0])
      {
        case 'n':
        {
          if (BreedingInProgress)
          {
            needBreedingInProgressMessage = true;
            break;
          }
          ::std::istringstream parameters(line);
          char command;
          ::std::string projectName;
          int population;
          ::std::string scenarioId;
          if (parameters >> command >> projectName >> population >> scenarioId)
          {
            ::std::string scenarioSettings;
            ::std::getline(parameters, scenarioSettings);
            if (Breeder::Failure::NONE == breeder.createProject(projectName, population, scenarioId, scenarioSettings))
            {
              prompt = projectName + DefaultPrompt;
            }
            else
            {
              ::std::cout << "creation project \"" << projectName << "\" failed\n";
              prompt = DefaultPrompt;
            }
          }
          else
          {
            needDescription = true;
          }
          break;
        }
        case 'l':
        {
          if (BreedingInProgress)
          {
            needBreedingInProgressMessage = true;
            break;
          }
          ::std::istringstream parameters(line);
          char command;
          ::std::string projectName;
          if (parameters >> command >> projectName)
          {
            if (Breeder::Failure::NONE == breeder.loadProject(projectName))
            {
              prompt = projectName + DefaultPrompt;
            }
            else
            {
              ::std::cout << "loading project \"" << projectName << "\" failed\n";
              prompt = DefaultPrompt;
            }
          }
          else
          {
            needDescription = true;
          }
          break;
        }
        case 'b':
        {
          if (BreedingInProgress)
          {
            needBreedingInProgressMessage = true;
            break;
          }
          ::std::istringstream parameters(line);
          char command;
          parameters >> command;
          {
            int steps(0);
            parameters >> steps;
            BreedingInProgress = true;
            ::std::thread(runBreeding, ::std::reference_wrapper(breeder), steps).detach();
          }
          break;
        }
        case 's':
        {
          if (BreedingInProgress)
          {
            StopBreeding = true;
          }
          else
          {
            ::std::cout << "no breeding is in progress\n";
          }
          break;
        }
        case 'i':
        {
          ::std::cout << "breeding is " << (BreedingInProgress ? "" : "NOT ") << "in progress\n";
          ::std::cout << "stop breeding is " << (StopBreeding ? "" : "NOT ") << "set\n";
          break;
        }
        case 'x':
          exit = true;
          StopBreeding = true;
          break;
        case '?':
          for (const auto& command : commandDescriptions)
          {
            ::std::cout << command.first << ": " << command.second << '\n';
          }
          break;
        case 'c':
        {
          ::std::istringstream parameters(line);
          char command;
          ::std::string scenarioId;
          parameters >> command >> scenarioId;
          ::std::string configHelp(scenarioFactory.getHelpText(scenarioId));
          if (configHelp.empty())
          {
            ::std::cout << "no config help for scenario " << scenarioId << '\n';
          }
          else
          {
            ::std::cout << configHelp << '\n';
          }
          break;
        }
        default:
          ::std::cout << "unrecognized command\n";
          break;
      }
      if (needDescription)
      {
        ::std::cout << commandDescriptions.at(line[0]) << '\n';
      }
      if (needBreedingInProgressMessage)
      {
        ::std::cout << "cannot execute: breeding in progress\n";
      }
    }
  }

  while (BreedingInProgress)
    ;
}
