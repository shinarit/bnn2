//
// OneVsOneParallelRunner.hpp
//
//  Created on: 2019. m�rc. 29.
//      Author: fasz
//

#ifndef H057D4005_4445_41F8_B908_25471AAF2024
#define H057D4005_4445_41F8_B908_25471AAF2024

#include <IScenario.hpp>

#include <vector>
#include <functional>

class ISpecimen;

class OneVsOneParallelRunner
{
public:
  struct Score
  {
    IScenario::Score p1Score;
    IScenario::Score p2Score;
  };
  typedef ::std::function<Score(ISpecimen* p1, ISpecimen* p2)> Processor;
  OneVsOneParallelRunner();

  ::std::vector<IScenario::Score> run(const ::std::vector<ISpecimen*>& breed, Processor processor);
};

#endif // H057D4005_4445_41F8_B908_25471AAF2024
