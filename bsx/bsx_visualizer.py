#!/usr/bin/env python
# -*- coding: utf-8 -*-

from tkinter import *
import sys
import itertools
import os

def Max(lhs, rhs):
  if lhs > rhs:
    return lhs
  else:
    return rhs

def Min(lhs, rhs):
  if lhs > rhs:
    return rhs
  else:
    return lhs

def CreateCanvas(master):
  canvas = Canvas(master)
  canvas.xview_moveto(0)
  canvas.yview_moveto(0)
  canvas["bg"] = "white"
  canvas["width"] = 1600
  canvas["height"] = 900
  
  return canvas

#
# ----------------------------------------------------------------------------------------
#

class Hexgrid(Frame):
  def __init__(self, tk, lines):
    super().__init__(tk)
    self.distance = int(lines[0].split(' ')[1])
    #self.distance = 6
    
    self.steps = []
    for line in lines[1:]:
      if "S0" == line[:2]:
        self.steps.append([])
      self.steps[-1].append(line)
    self.currentStep = 1
  
    self.prevButton = Button(self, text="<-", command=self.PrevStage)
    self.prevButton.grid(row=0, column=0, sticky=E)
    self.nextButton = Button(self, text="->", command=self.NextStage)
    self.nextButton.grid(row=0, column=1, sticky=W)
    self.pageLabel = Label(self, text="1/%d" % len(self.steps))
    self.pageLabel.grid(row=1, columnspan=2)
    self.canvas = CreateCanvas(self)
    self.canvas.grid(columnspan=2)
    
    #or tk?
    self.bind('<Left>', lambda event: self.PrevStage())
    self.bind('<Right>', lambda event: self.NextStage())
    self.bind('<Home>', lambda event: self.FirstStage())
    self.bind('<End>', lambda event: self.LastStage())
    
    self.width = int(self.canvas.cget('width'))
    self.height = int(self.canvas.cget('height'))
    self.sqrt3 = 3 ** (1 / 2)
    xNum = (self.distance + 1) * self.sqrt3
    yNum = (self.distance + 1) * 2
    
    self.fieldsize = Min(self.width / xNum, self.height / yNum)
    self.markersize = self.fieldsize / 2

    for x in range(self.distance // 2):
      for y in range(-x, self.distance // 2 + 1):
        pos = self.GetCoords((x, y))
        self.DrawField(pos[0], pos[1], "gray", "%d;%d" % (x, y))
    for x in range(self.distance // 2, self.distance + 1):
      for y in range(-(self.distance // 2), self.distance - x + 1):
        pos = self.GetCoords((x, y))
        self.DrawField(pos[0], pos[1], "gray", "%d;%d" % (x, y))

    self.stepObjects = []
    self.VisualizeSteps()

  def VisualizeSteps(self):
    fillColors = {
      "S0": "green",
      "S1": "red",
      "B": "black"
      }
    for obj in self.stepObjects:
      self.canvas.delete(obj)
    for line in self.steps[self.currentStep - 1]:
      print(line)
      parts = line.split(' ')
      currentPos = [int(x) for x in parts[2].split(';')]
      coords = self.GetCoords(currentPos)
      self.stepObjects.append(self.canvas.create_oval((coords[0] - self.markersize / 2,
                                                        coords[1] - self.markersize / 2,
                                                        coords[0] + self.markersize / 2,
                                                        coords[1] + self.markersize / 2),
                                                      fill=fillColors[parts[0]]))

  def UpdateLabel(self):
    self.pageLabel.config(text="%d/%d" % (self.currentStep, len(self.steps)))    

  def FirstStage(self):
    if 1 != self.currentStep:
      self.currentStep = 1
      self.UpdateLabel()
      self.VisualizeSteps()      

  def LastStage(self):
    if len(self.steps) != self.currentStep:
      self.currentStep = len(self.steps)
      self.UpdateLabel()
      self.VisualizeSteps()      

  def NextStage(self):
    if self.currentStep < len(self.steps):
      self.currentStep += 1
      self.UpdateLabel()
      self.VisualizeSteps()

  def PrevStage(self):
    if self.currentStep > 1:
      self.currentStep -= 1
      self.UpdateLabel()
      self.VisualizeSteps()

  def DrawField(self, x, y, fill, txt):
    size = self.fieldsize
    self.canvas.create_polygon([(x,                         y + size),
                                (x + self.sqrt3 * size / 2, y + size / 2),
                                (x + self.sqrt3 * size / 2, y - size / 2),
                                (x,                         y - size),
                                (x - self.sqrt3 * size / 2, y - size / 2),
                                (x - self.sqrt3 * size / 2, y + size / 2)], fill = fill, outline = "black")
    #self.canvas.create_text((x, y), text = txt)

  def FindPos(self, x, y):
    pos = min((pow(x - self.GetCoords(pos)[0] - self.fieldsize/2, 2) + pow(y - self.GetCoords(pos)[1] - self.fieldsize/2, 2), pos) for pos in self.table.iterkeys())[1]
    if (pow(x - self.GetCoords(pos)[0] - self.fieldsize/2, 2) + pow(y - self.GetCoords(pos)[1] - self.fieldsize/2, 2)) <= pow(self.fieldsize / 2, 2):
      return pos
    else:
      return None

  def FindField(self, pos):
    return self.table.get(pos)

  """  def GetSteps(self, pos):
    if 0 == pos[1] % 2:
      return ((1, 0), (0, 1), (-1, 1), (-1, 0), (-1, -1), (0, -1))
    else:
      return ((1, 0), (0, 1), (1, 1), (-1, 0), (1, -1), (0, -1))
"""
  def GetCoords(self, pos):
    xDiff = self.sqrt3 / 2
    yDiff = 3/2
    return (self.fieldsize * xDiff * (pos[0] - pos[1]) - self.fieldsize * xDiff * (self.distance / 2 + 1) + self.width / 2,
            self.fieldsize * yDiff * (-pos[0] - pos[1]) + self.fieldsize * yDiff * self.distance / 2 + self.height / 2)

class Visualizer():
  def __init__(self, tk, breedingPath):
    self.tk = tk
    self.path = breedingPath
    steps = sorted([dir for dir in os.listdir(breedingPath) if os.path.isdir(os.path.join(breedingPath, dir))])[:-1]

    self.selectedStep = StringVar(self.tk)
    self.stepSelector = OptionMenu(self.tk, self.selectedStep, *steps)
    self.selectedStep.trace('w', self.StepSelected)
    self.stepSelector.grid(row=0, column=0)
    
    self.visualize = Button(self.tk, text="show", command=self.VisualizeFight)
    self.visualize.grid(row=0, column=3)
    
  def StepSelected(self, *args):
    with open(os.path.join(os.path.join(self.path, self.selectedStep.get()), "breedinglogs")) as brLogs:
      lines = brLogs.read().split('\n')
    self.playerCount = int(lines[0].split(' ')[1])
    self.scores = [[int(x) for x in line.split(':')] for line in lines[2:self.playerCount + 2]]
    
    with open(os.path.join(os.path.join(self.path, self.selectedStep.get()), "scoring")) as matchLogs:
      self.matches = matchLogs.read().split('playing')[1:]

    if not hasattr(self, 'p1Selector'):
      self.p1Selector = Entry(self.tk)
      self.p1Selector.grid(row=0, column=1)
      self.p2Selector = Entry(self.tk)
      self.p2Selector.grid(row=0, column=2)
  
  def VisualizeFight(self):
    if hasattr(self, 'visualizer'):
      self.visualizer.destroy()
    n = int(self.p1Selector.get())
    m = int(self.p2Selector.get())
    if m < n:
      m, n = n, m
    bigStep = self.playerCount * (self.playerCount - 1) // 2 - (self.playerCount - n) * ((self.playerCount - n) - 1) //2
    smallStep = m - n - 1
    self.visualizer = Hexgrid(self.tk, self.matches[bigStep + smallStep].split('\n')[1:])
    self.visualizer.grid(row=1, columnspan=4)

if __name__ == "__main__":
  tk = Tk()
  Visualizer(tk, sys.argv[1])
  tk.mainloop()
