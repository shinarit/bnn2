//
// BattleshipNeuralController.cpp
//
//  Created on: 2019. m�rc. 13.
//      Author: fasz
//

#include "BattleshipXNeuralController.hpp"

#include <GenericRecurrentNetwork.hpp>

#include <cstring>

namespace
{
// unsigned cubeDist(const BattleshipX::Position& lhs, const BattleshipX::Position& rhs)
//{
//  return ::std::max(::std::abs(lhs.x - rhs.x), ::std::max(::std::abs(lhs.y - rhs.y), ::std::abs(lhs.z - rhs.z)));
//}

BattleshipX::Speed positionToSpeed(BattleshipX::Position pos)
{
  int distance(::std::max(::std::abs(pos.x), ::std::max(::std::abs(pos.y), ::std::abs(pos.z))));
  //  if x == distance:
  //    direction = distance - y
  //  elif z == -distance:
  //    direction = x
  //  elif x == -distance:
  //    direction = distance * 4 + y
  //  elif y == distance:
  //    direction = distance * 5 - z
  //  elif y == -distance:
  //    direction = distance * 2 + z
  //  elif z == distance:
  //    direction = distance * 3 - x
  //  return (distance, direction)

  if (pos.x == distance)
  {
    return {static_cast<unsigned>(distance), static_cast<unsigned>(distance - pos.y)};
  }
  else if (pos.z == -distance)
  {
    return {static_cast<unsigned>(distance), static_cast<unsigned>(pos.x)};
  }
  else if (pos.x == -distance)
  {
    return {static_cast<unsigned>(distance), static_cast<unsigned>(distance * 4 + pos.y)};
  }
  else if (pos.y == distance)
  {
    return {static_cast<unsigned>(distance), static_cast<unsigned>(distance * 5 - pos.z)};
  }
  else if (pos.y == -distance)
  {
    return {static_cast<unsigned>(distance), static_cast<unsigned>(distance * 2 + pos.z)};
  }
  else if (pos.z == distance)
  {
    return {static_cast<unsigned>(distance), static_cast<unsigned>(distance * 3 - pos.x)};
  }
  throw - 1;
}
} // namespace

BattleshipXNeuralController::BattleshipXNeuralController(GenericRecurrentNetwork& network, unsigned bulletCount)
  : mNetwork(network), mBulletCount(bulletCount), mInput(3 + mBulletCount * 3)
{
  if (mInput.size() != network.inputNumber() ||
    // turn, accelerate and fire
    1 + 1 + 1 != network.outputNumber())
  {
    throw InvalidNetworkException();
  }
}

namespace
{
BattleshipX::Side getSide(int input)
{
  if (input > 32)
    return BattleshipX::Side::LEFT;
  else if (input < -32)
    return BattleshipX::Side::RIGHT;
  return BattleshipX::Side::CENTER;
}
BattleshipX::Acceleration getAcceleration(int input)
{
  if (input > 32)
    return BattleshipX::Acceleration::POSITIVE;
  else if (input < -32)
    return BattleshipX::Acceleration::NEGATIVE;
  return BattleshipX::Acceleration::ZERO;
}

BattleshipX::Position rotateToPerspective(BattleshipX::Position referencePosition,
  BattleshipX::Speed referenceDirection,
  BattleshipX::Position position)
{
  BattleshipX::Position offset{
    position.x - referencePosition.x, position.y - referencePosition.y, position.z - referencePosition.z};
  BattleshipX::Speed offsetSpeed(positionToSpeed(offset));
  BattleshipX::Speed adjustedReferenceDirection(referenceDirection);
  adjustedReferenceDirection.accelerate(int(offsetSpeed.size) - int(adjustedReferenceDirection.size));
  offsetSpeed.turn(BattleshipX::Side::LEFT, adjustedReferenceDirection.directional);
  return offsetSpeed.toOffset();
}

const int MarkMaxValue = 256;
int limitToMark(int value)
{
  if (value < -MarkMaxValue)
  {
    return -MarkMaxValue;
  }
  else if (value > MarkMaxValue)
  {
    return MarkMaxValue;
  }
  return value;
}
} // namespace

BattleshipX::Controller::Command BattleshipXNeuralController::produceCommand(BattleshipX::Position myPosition,
  BattleshipX::Speed myDirection,
  BattleshipX::Position enemyPosition,
  const ::std::vector<BattleshipX::Position>& bullets)
{
  ::std::memset(mInput.data(), 0, sizeof(decltype(mInput[0])) * mInput.size());

  enemyPosition = rotateToPerspective(myPosition, myDirection, enemyPosition);
  mInput[0] = limitToMark(enemyPosition.x);
  mInput[1] = limitToMark(enemyPosition.y);
  mInput[2] = limitToMark(enemyPosition.z);

  for (unsigned i(0); i < ::std::min<unsigned>(mBulletCount, bullets.size()); ++i)
  {
    BattleshipX::Position bulletPos(rotateToPerspective(myPosition, myDirection, bullets[i]));
    mInput[3 * (i + 1) + 0] = limitToMark(bulletPos.x);
    mInput[3 * (i + 1) + 1] = limitToMark(bulletPos.y);
    mInput[3 * (i + 1) + 2] = limitToMark(bulletPos.z);
  }

  mNetwork.step(mInput);

  return {getSide(mNetwork.getOutput(0)), getSide(mNetwork.getOutput(1)), getAcceleration(mNetwork.getOutput(2))};
}
