//
// Scenario.cpp
//
//  Created on: 2019. m�rc. 9.
//      Author: fasz
//

#include <GenericRecurrentNetwork.hpp>
#include "BattleshipX.hpp"
#include "BattleshipXNeuralController.hpp"
#include "BattleshipXScenario.hpp"

#include <iostream>
#include <sstream>

const ::std::string BattleshipXScenario::Config::HelpText
  = "<starting distance of ships> <number of hidden nodes> <number of bullets to register> <length of matches>"
    "<scoring: Win Loss Draw Timeout> <match config: bulletspeed bulletlifetime reloadtime shipspeedlimit>";

bool BattleshipXScenario::Config::configure(::std::string settings)
{
  ::std::istringstream in(settings);
  return !(in >> startingDistance >> hiddenNodeCount >> bulletDetectionCount >> matchRoundLimit >> scoring.win
    >> scoring.loss >> scoring.draw >> scoring.timeout >> bsxConfig.bulletSpeed >> bsxConfig.bulletLifetime
    >> bsxConfig.shipReloadTime >> bsxConfig.shipSpeedLimit)
            .fail();
}

BattleshipXScenario::BattleshipXScenario(Config config): mConfig(config)
{
}

::std::vector<IScenario::Score>
BattleshipXScenario::scoreBreed(::std::vector<ISpecimen*> breed, ::std::mt19937&, ::std::ostream& scoringLogger) const
{
  ::std::vector<IScenario::Score> result(breed.size(), IScenario::Score());
#ifdef PARALLEL_SCORING
  result = parallelRunner.run(breed,
    ::std::bind(parallelProcess,
      this,
      ::std::placeholders::_1,
      ::std::placeholders::_2,
      ::std::reference_wrapper(scoringLogger)));

  return result;
#else
  ::std::vector<GenericRecurrentNetwork*> convertedBreed;
  ::std::transform(breed.begin(), breed.end(), ::std::back_inserter(convertedBreed), [](ISpecimen* specimen) {
    GenericRecurrentNetwork* network(dynamic_cast<GenericRecurrentNetwork*>(specimen));
    if (nullptr == network)
      throw InvalidSpecimenException();
    return network;
  });
  for (unsigned i(0); i < convertedBreed.size() - 1; ++i)
  {
    BattleshipXNeuralController playerOne(*convertedBreed[i], mConfig.bulletDetectionCount);
    for (unsigned j(i + 1); j < convertedBreed.size(); ++j)
    {
      scoringLogger << "playing " << i << " vs " << j << '\n';
      BattleshipXNeuralController playerTwo(*convertedBreed[j], mConfig.bulletDetectionCount);
      convertedBreed[i]->resetMemory();
      convertedBreed[j]->resetMemory();

      auto scores(playMatch(playerOne, playerTwo, scoringLogger));

      result[i] += scores.p1;
      result[j] += scores.p2;
    }
  }
#endif
  return result;
}

ISpecimen* BattleshipXScenario::createSpecimen(::std::mt19937& randomEngine) const
{
  ISpecimen* newBoi(new GenericRecurrentNetwork(3 + mConfig.bulletDetectionCount * 3, mConfig.hiddenNodeCount, 3));
  newBoi->mutate(randomEngine);
  return newBoi;
}

BattleshipXScenario::ScorePair BattleshipXScenario::playMatch(BattleshipXNeuralController& p1,
  BattleshipXNeuralController& p2,
  ::std::ostream& scoringLogger) const
{
  BattleshipX match(mConfig.startingDistance,
    mConfig.bsxConfig.bulletSpeed,
    mConfig.bsxConfig.bulletLifetime,
    mConfig.bsxConfig.shipReloadTime,
    mConfig.bsxConfig.shipSpeedLimit,
    p1,
    p2,
    scoringLogger);
  unsigned turn(0);
  BattleshipX::Result state(BattleshipX::Result::ONGOING);
  while (mConfig.matchRoundLimit > turn++ && (BattleshipX::Result::ONGOING == (state = match.step())))
    ;
  IScenario::Score p1Score;
  IScenario::Score p2Score;
  switch (state)
  {
    case BattleshipX::Result::ONGOING:
      p1Score = mConfig.scoring.timeout;
      p2Score = mConfig.scoring.timeout;
      break;
    case BattleshipX::Result::DRAW:
      p1Score = mConfig.scoring.draw;
      p2Score = mConfig.scoring.draw;
      break;
    case BattleshipX::Result::PLAYER_ONE_WINS:
      p1Score = mConfig.scoring.win;
      p2Score = mConfig.scoring.loss;
      break;
    case BattleshipX::Result::PLAYER_TWO_WINS:
      p1Score = mConfig.scoring.loss;
      p2Score = mConfig.scoring.win;
      break;
  }
  return {p1Score, p2Score};
}

#ifdef PARALLEL_SCORING
OneVsOneParallelRunner::Score
BattleshipXScenario::parallelProcess(ISpecimen* p1, ISpecimen* p2, ::std::ostream& scoringLogger) const
{
  BattleshipXNeuralController p1Controller(*static_cast<GenericRecurrentNetwork*>(p1), mConfig.bulletDetectionCount);
  BattleshipXNeuralController p2Controller(*static_cast<GenericRecurrentNetwork*>(p2), mConfig.bulletDetectionCount);
  ::std::ostringstream log;
  const BattleshipXScenario::ScorePair score(playMatch(p1Controller, p2Controller, log));

  {
    ::std::unique_lock<::std::mutex> lock(mMatchLogMutex);
    scoringLogger << "playing ? vs ?\n" << log.str();
  }
  return {score.p1, score.p2};
}
#endif
