#ifndef BATTLESHIPX_HPP_
#define BATTLESHIPX_HPP_

#include <functional>
#include <vector>
#include <optional>
#include <iosfwd>
#include <random>

// hexagon based map
class BattleshipX
{
public:
  class InternalErrorException
  {
  };
  class InvalidSpeedException : public InternalErrorException
  {
  };
  enum class Side
  {
    LEFT,
    RIGHT,
    CENTER
  };
  enum class Acceleration
  {
    POSITIVE,
    NEGATIVE,
    ZERO
  };
  struct Position
  {
    int x, y, z;
  };
  struct Speed
  {
    unsigned size;
    unsigned directional;

    Position toOffset() const;
    void turn(Side side, unsigned amount = 1);
    // safe
    bool accelerate(Acceleration acc, unsigned speedLimit);
    // unsafe
    void accelerate(int amount);
  };
  class Controller
  {
  public:
    struct Command
    {
      Side turn;
      Side shoot;
      Acceleration acceleration;
    };
    virtual Command produceCommand(Position myPosition,
      Speed myDirection,
      Position enemyPosition,
      const ::std::vector<Position>& bullets)
      = 0;
  };
  struct Dimensions
  {
    unsigned width, height;
  };
  BattleshipX(int startingDistance,
    unsigned bulletSpeed,
    unsigned bulletLifetime,
    unsigned shipReloadTime,
    unsigned shipSpeedLimit,
    Controller& playerOne,
    Controller& playerTwo,
    ::std::ostream& logger);
  BattleshipX(const BattleshipX&) = delete;
  BattleshipX& operator=(const BattleshipX&) = delete;

  enum class Result
  {
    ONGOING,
    PLAYER_ONE_WINS,
    PLAYER_TWO_WINS,
    DRAW
  };

  Result step();

private:
  struct Ship
  {
    Position position;
    Speed speed;
    unsigned reloadTimer = 0;
  };
  struct Bullet
  {
    Position position;
    Speed speed;
    unsigned lifetime;
  };

  void adjustBullet(Bullet& bullet, Side direction);

  Ship mShips[2];
  using controllerRef = ::std::reference_wrapper<Controller>;
  controllerRef mControllers[2];
  ::std::vector<Bullet> mBullets;

  const unsigned mBulletSpeed;
  const unsigned mBulletLifetime;
  const unsigned mShipReloadTime;
  const unsigned mShipSpeedLimit;

  ::std::ostream& mLogger;
};

#endif
