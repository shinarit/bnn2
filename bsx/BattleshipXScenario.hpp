//
// Scenario.hpp
//
//  Created on: 2019. m�rc. 9.
//      Author: fasz
//

#ifndef H6733B553_532C_4E52_BF0C_395452BCB119
#define H6733B553_532C_4E52_BF0C_395452BCB119

#include <IScenario.hpp>

#ifdef PARALLEL_SCORING
#include <OneVsOneParallelRunner.hpp>

#include <mutex>
#endif

class BattleshipXNeuralController;

class BattleshipXScenario : public IScenario
{
public:
  class InvalidSpecimenException
  {
  };
  struct Scoring
  {
    IScenario::Score win;
    IScenario::Score loss;
    IScenario::Score draw;
    IScenario::Score timeout;
  };
  struct BattleshipXConfig
  {
    unsigned bulletSpeed;
    unsigned bulletLifetime;
    unsigned shipReloadTime;
    unsigned shipSpeedLimit;
  };
  struct Config
  {
    bool configure(::std::string settings);

    static const ::std::string HelpText;

    unsigned startingDistance;
    unsigned hiddenNodeCount;
    unsigned bulletDetectionCount;
    unsigned matchRoundLimit;
    Scoring scoring;
    BattleshipXConfig bsxConfig;
  };

  BattleshipXScenario(Config config);

  virtual ::std::vector<IScenario::Score> scoreBreed(::std::vector<ISpecimen*> breed,
    ::std::mt19937& randomEngine,
    ::std::ostream& scoringLogger) const override;
  virtual ISpecimen* createSpecimen(::std::mt19937& randomEngine) const override;

private:
  struct ScorePair
  {
    IScenario::Score p1;
    IScenario::Score p2;
  };
  ScorePair
  playMatch(BattleshipXNeuralController& p1, BattleshipXNeuralController& p2, ::std::ostream& scoringLogger) const;
#ifdef PARALLEL_SCORING
  OneVsOneParallelRunner::Score parallelProcess(ISpecimen* p1, ISpecimen* p2, ::std::ostream& scoringLogger) const;
  mutable OneVsOneParallelRunner parallelRunner;

  mutable ::std::mutex mMatchLogMutex;
  ::std::vector<::std::string> mMatchLogCollector;
#endif
  const Config mConfig;
};

#endif // H6733B553_532C_4E52_BF0C_395452BCB119
