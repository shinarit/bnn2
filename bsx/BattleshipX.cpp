#include "BattleshipX.hpp"

#include <flatset.hpp>

#include <iostream>

bool operator<(const BattleshipX::Position& lhs, const BattleshipX::Position& rhs)
{
  return lhs.x < rhs.x || (lhs.x == rhs.x && lhs.y < rhs.y) || (lhs.x == rhs.x && lhs.y == rhs.y && lhs.z < rhs.z);
}

namespace
{
BattleshipX::Position operator+(const BattleshipX::Position& lhs, const BattleshipX::Position& rhs)
{
  return {lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z};
}

::std::ostream& operator<<(::std::ostream& out, const BattleshipX::Position& pos)
{
  return out << pos.x << ';' << pos.y << ';' << pos.z;
}

void collectPath(const BattleshipX::Position& position,
  BattleshipX::Speed speed,
  ::flat::flat_set<BattleshipX::Position>& cont)
{
  do
  {
    cont.insert(position + speed.toOffset());
  } while (speed.accelerate(BattleshipX::Acceleration::NEGATIVE, 0));
}
} // namespace

BattleshipX::BattleshipX(int startingDistance,
  unsigned bulletSpeed,
  unsigned bulletLifetime,
  unsigned shipReloadTime,
  unsigned shipSpeedLimit,
  Controller& playerOne,
  Controller& playerTwo,
  ::std::ostream& logger)
  : mShips{{{0, 0, 0}, {1, 1}}, {{startingDistance, 0, -startingDistance}, {1, 4}}}
  , mControllers{playerOne, playerTwo}
  , mBullets()
  , mBulletSpeed(bulletSpeed)
  , mBulletLifetime(bulletLifetime)
  , mShipReloadTime(shipReloadTime)
  , mShipSpeedLimit(shipSpeedLimit)
  , mLogger(logger)
{
  mLogger << "M " << startingDistance << '\n';
}

BattleshipX::Result BattleshipX::step()
{
  // get input
  ::std::vector<Position> bulletPositions;
  ::std::transform(mBullets.begin(), mBullets.end(), ::std::back_inserter(bulletPositions), [](const Bullet& bullet) {
    return bullet.position;
  });

  for (int i(0); i < 2; ++i)
  {
    Ship& currentShip(mShips[i]);
    const Ship& otherShip(mShips[1 - i]);

    Controller::Command command(mControllers[i].get().produceCommand(
      currentShip.position, currentShip.speed, otherShip.position, bulletPositions));
    currentShip.speed.turn(command.turn, mShipSpeedLimit);
    currentShip.speed.accelerate(command.acceleration, mShipSpeedLimit);
    auto shootDirection(command.shoot);
    if (Side::CENTER != shootDirection && 0 == currentShip.reloadTimer)
    {
      currentShip.reloadTimer = mShipReloadTime;
      Bullet shotFired{currentShip.position, currentShip.speed, mBulletLifetime};
      adjustBullet(shotFired, shootDirection);
      mBullets.push_back(shotFired);
    }
    if (currentShip.reloadTimer)
      --currentShip.reloadTimer;
  }

  // move and collide shit
  {
    // bullets can't collide, ships can
    ::flat::flat_set<Position> p1PathPoints;
    ::flat::flat_set<Position> p2PathPoints;
    collectPath(mShips[0].position, mShips[0].speed, p1PathPoints);
    collectPath(mShips[1].position, mShips[1].speed, p2PathPoints);
    for (int i(0); i < 2; ++i)
    {
      const Position prevPos(mShips[i].position);
      mShips[i].position = mShips[i].position + mShips[i].speed.toOffset();
      mLogger << "S" << i << ' ' << mShips[i].reloadTimer << ' ' << prevPos << " -> " << mShips[i].position << '\n';
    }

    ::flat::flat_set<Position> bulletPathPoints;
    for (Bullet& bullet : mBullets)
    {
      collectPath(bullet.position, bullet.speed, bulletPathPoints);
      const Position prevPos(bullet.position);
      bullet.position = bullet.position + bullet.speed.toOffset();
      mLogger << "B " << bullet.lifetime << ' ' << prevPos << " -> " << bullet.position << '\n';
    }

    bool died[] = {false, false};
    for (const Position& pos : p1PathPoints)
    {
      if (!died[0] && 1 == bulletPathPoints.count(pos))
      {
        died[0] = true;
      }
      if (1 == p2PathPoints.count(pos))
      {
        died[0] = died[1] = true;
        break;
      }
    }
    if (!died[1])
    {
      for (const Position& pos : p2PathPoints)
      {
        if (!died[1] && 1 == bulletPathPoints.count(pos))
        {
          died[1] = true;
          break;
        }
      }
    }
    for (unsigned i(0); i < mBullets.size();)
    {
      if (0 != mBullets[i].lifetime && 0 == --mBullets[i].lifetime)
      {
        mBullets.erase(mBullets.begin() + i);
      }
      else
      {
        ++i;
      }
    }
    //    auto bIt(bulletPathPoints.begin());
    //    auto p1It(p1PathPoints.begin());
    //    auto p2It(p2PathPoints.begin());
    //
    //    while ((p1PathPoints.end() != p1It || p2PathPoints.end() != p2It))
    //    {
    // fast algorithm on hold for now
    //    }
    if (died[0] || died[1])
    {
      if (died[0] && died[1])
        return Result::DRAW;
      if (died[0])
        return Result::PLAYER_TWO_WINS;
      if (died[1])
        return Result::PLAYER_ONE_WINS;
    }
  }

  return Result::ONGOING;
}

BattleshipX::Position BattleshipX::Speed::toOffset() const
{
  int direction(directional);
  int speed(size);
  switch (direction / speed)
  {
    case 0:
      return {-direction, speed - direction, -speed};
    case 1:
      return {speed, speed - direction, direction - speed * 2};
    case 2:
      return {speed * 3 - direction, -speed, direction - speed * 2};
    case 3:
      return {speed * 3 - direction, direction - speed * 4, speed};
    case 4:
      return {-speed, direction - speed * 4, speed * 5 - direction};
    case 5:
      return {direction - speed * 6, speed, speed * 5 - direction};
    default:
      throw InvalidSpeedException();
  }
}

void BattleshipX::Speed::turn(Side side, unsigned amount)
{
  switch (side)
  {
    case BattleshipX::Side::LEFT:
      directional = (directional + size * 6 - amount) % size;
      break;
    case BattleshipX::Side::RIGHT:
      directional = (directional + amount) % size;
      break;
    default:
      break;
  }
}

bool BattleshipX::Speed::accelerate(Acceleration acc, unsigned speedLimit)
{
  switch (acc)
  {
    case BattleshipX::Acceleration::NEGATIVE:
      if (size > 1)
      {
        accelerate(-1);
        return true;
      }
      break;
    case BattleshipX::Acceleration::POSITIVE:
      if (size < speedLimit)
      {
        accelerate(1);
        return true;
      }
      break;
    default:
      break;
  }
  return false;
}

void BattleshipX::Speed::accelerate(int amount)
{
  directional = (directional * (size + amount) + size / 2) / size;
  size += amount;
}

void BattleshipX::adjustBullet(Bullet& bullet, Side direction)
{
  Speed& speed(bullet.speed);
  speed.accelerate(int(mBulletSpeed) - int(speed.size));
  // 90 degree turn
  speed.turn(direction, speed.size * 1.5);
}

// template<class T>
// void turn(T& object, Battleship::Side side)
//{
//}
