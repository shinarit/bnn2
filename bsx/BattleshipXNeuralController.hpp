//
// BattleshipNeuralController.hpp
//
//  Created on: 2019. m�rc. 13.
//      Author: fasz
//

#ifndef H690FF6F9_CA0B_4B67_89F6_F46AA3563494
#define H690FF6F9_CA0B_4B67_89F6_F46AA3563494

#include "BattleshipX.hpp"

class GenericRecurrentNetwork;

class BattleshipXNeuralController : public BattleshipX::Controller
{
public:
  class InvalidNetworkException
  {
  };
  BattleshipXNeuralController(GenericRecurrentNetwork& network, unsigned bulletCount);

  virtual Command produceCommand(BattleshipX::Position myPosition,
    BattleshipX::Speed myDirection,
    BattleshipX::Position enemyPosition,
    const ::std::vector<BattleshipX::Position>& bullets) override;

private:
  GenericRecurrentNetwork& mNetwork;
  const unsigned mBulletCount;

  ::std::vector<int> mInput;
};

#endif // H690FF6F9_CA0B_4B67_89F6_F46AA3563494
