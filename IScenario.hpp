//
// IScenario.hpp
//
//  Created on: 2019. m�rc. 9.
//      Author: fasz
//

#ifndef HF135468C_CA7E_4191_9E2E_843603710FDB
#define HF135468C_CA7E_4191_9E2E_843603710FDB

#include <vector>
#include <iosfwd>
#include <random>

class ISpecimen;

class IScenario
{
public:
  virtual ~IScenario()
  {
  }

  typedef int Score;
  virtual ::std::vector<Score>
  scoreBreed(::std::vector<ISpecimen*> breed, ::std::mt19937& randomEngine, ::std::ostream& scoringLogger) const = 0;
  virtual ISpecimen* createSpecimen(::std::mt19937& randomEngine) const = 0;
  virtual ISpecimen* createSpecimen(::std::istream& in) const;
};

#endif // HF135468C_CA7E_4191_9E2E_843603710FDB
