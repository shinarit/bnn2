//
// ScenarioFactory.cpp
//
//  Created on: 2019. m�rc. 10.
//      Author: fasz
//

#include <ScenarioFactory.hpp>

#include <bs/BattleshipScenario.hpp>
#include <bsx/BattleshipXScenario.hpp>

#include <sstream>

namespace
{
template<class Scenario>
IScenario* create(::std::string settings)
{
  typename Scenario::Config config;
  if (config.configure(settings))
  {
    return new Scenario(config);
  }
  return nullptr;
}

template<class Scenario>
::std::string helpText()
{
  return Scenario::Config::HelpText;
}
} // namespace

IScenario* ScenarioFactory::createScenario(::std::string id, ::std::string settings) const
{
  if ("bs" == id)
  {
    return create<BattleshipScenario>(settings);
  }
  else if ("bsx" == id)
  {
    return create<BattleshipXScenario>(settings);
  }
  return nullptr;
}

::std::string ScenarioFactory::getHelpText(::std::string id) const
{
  if ("bs" == id)
  {
    return helpText<BattleshipScenario>();
  }
  else if ("bsx" == id)
  {
    return helpText<BattleshipXScenario>();
  }
  return "";
}
