//
// Scenario.cpp
//
//  Created on: 2019. m�rc. 9.
//      Author: fasz
//

#include <GenericRecurrentNetwork.hpp>
#include "Battleship.hpp"
#include "BattleshipNeuralController.hpp"
#include "BattleshipScenario.hpp"

#include <iostream>
#include <sstream>

const ::std::string BattleshipScenario::Config::HelpText
  = "<sizeX> <sizeY> <number of hidden nodes> <length of matches>";

bool BattleshipScenario::Config::configure(::std::string settings)
{
  ::std::istringstream in(settings);
  return (in >> size.x >> size.y >> hiddenNodeCount >> matchLength).fail();
}

BattleshipScenario::BattleshipScenario(Config config) // Size size, unsigned hiddenNodeCount, unsigned matchRoundLimit)
  : mConfig(config)
{
}

::std::vector<IScenario::Score> BattleshipScenario::scoreBreed(::std::vector<ISpecimen*> breed,
  ::std::mt19937& randomEngine,
  ::std::ostream& scoringLogger) const
{
  ::std::vector<GenericRecurrentNetwork*> convertedBreed;
  ::std::transform(breed.begin(), breed.end(), ::std::back_inserter(convertedBreed), [](ISpecimen* specimen) {
    GenericRecurrentNetwork* network(dynamic_cast<GenericRecurrentNetwork*>(specimen));
    if (nullptr == network)
      throw InvalidSpecimenException();
    return network;
  });
  ::std::vector<IScenario::Score> result(convertedBreed.size(), 0);
  for (unsigned i(0); i < convertedBreed.size() - 1; ++i)
  {
    for (unsigned j(i + 1); j < convertedBreed.size(); ++j)
    {
      scoringLogger << "playing " << i << " vs " << j << '\n';
      Battleship::Dimensions mapSize{mConfig.size.x, mConfig.size.y};
      BattleshipNeuralController playerOne(*convertedBreed[i], mapSize);
      BattleshipNeuralController playerTwo(*convertedBreed[j], mapSize);
      Battleship match(mapSize, playerOne, playerTwo, randomEngine, scoringLogger);
      unsigned turn(0);
      Battleship::Result state(Battleship::Result::ONGOING);
      while (mConfig.matchLength > turn++ && (Battleship::Result::ONGOING == (state = match.step())))
        ;
      IScenario::Score p1Score;
      IScenario::Score p2Score;
      switch (state)
      {
        case Battleship::Result::ONGOING:
          p1Score = TIMEOUT_SCORE;
          p2Score = TIMEOUT_SCORE;
          break;
        case Battleship::Result::DRAW:
          p1Score = DRAW_SCORE;
          p2Score = DRAW_SCORE;
          break;
        case Battleship::Result::PLAYER_ONE_WINS:
          p1Score = WIN_SCORE;
          p2Score = LOSE_SCORE;
          break;
        case Battleship::Result::PLAYER_TWO_WINS:
          p1Score = LOSE_SCORE;
          p2Score = WIN_SCORE;
          break;
      }
      result[i] += p1Score;
      result[j] += p2Score;
    }
  }
  return result;
}

ISpecimen* BattleshipScenario::createSpecimen(::std::mt19937& randomEngine) const
{
  ISpecimen* newBoi(new GenericRecurrentNetwork(mConfig.size.x * mConfig.size.y, mConfig.hiddenNodeCount, 2));
  newBoi->mutate(randomEngine);
  // ?
  return newBoi;
}
