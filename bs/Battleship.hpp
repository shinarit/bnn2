#ifndef BATTLESHIP_HPP_
#define BATTLESHIP_HPP_

#include <functional>
#include <vector>
#include <optional>
#include <iosfwd>
#include <random>

// hexagon based map
class Battleship
{
public:
  enum class Side
  {
    LEFT,
    RIGHT
  };
  struct Position
  {
    unsigned x, y;
  };
  class Controller
  {
  public:
    struct Command
    {
      ::std::optional<Side> turn;
      ::std::optional<Side> shoot;
    };
    virtual Command produceCommand(Position myPosition, Position enemyPosition, const ::std::vector<Position>& bullets)
      = 0;
  };
  struct Dimensions
  {
    unsigned width, height;
  };
  Battleship(Dimensions dimensions,
    Controller& playerOne,
    Controller& playerTwo,
    ::std::mt19937& mt,
    ::std::ostream& logger);
  Battleship(const Battleship&) = delete;
  Battleship& operator=(const Battleship&) = delete;

  enum class Result
  {
    ONGOING,
    PLAYER_ONE_WINS,
    PLAYER_TWO_WINS,
    DRAW
  };

  Result step();

private:
  enum class Direction
  {
    NW,
    NE,
    E,
    SE,
    SW,
    W
  };
  struct Ship
  {
    unsigned x, y;
    Direction direction;
    unsigned reloadTimer = 0;
    static const unsigned RELOAD_TIME = 4;
  };
  struct Bullet
  {
    unsigned x, y;
    Direction direction;
    unsigned lifetime;
    static const unsigned LIFETIME = 0; // 0 == infinity
  };

  template<class T>
  friend void turn(T& object, Battleship::Side side);
  template<class T>
  bool moveUp(T& object)
  {
    if (object.y < mDimensions.height - 1)
    {
      ++object.y;
      return false;
    }
    return true;
  }
  template<class T>
  bool moveRight(T& object)
  {
    if (object.x < mDimensions.width - 1)
    {
      ++object.x;
      return false;
    }
    return true;
  }
  template<class T>
  bool moveLeft(T& object)
  {
    if (object.x > 0)
    {
      --object.x;
      return false;
    }
    return true;
  }
  template<class T>
  bool moveDown(T& object)
  {
    if (object.y > 0)
    {
      --object.y;
      return false;
    }
    return true;
  }
  template<class T>
  bool move(T& object)
  {
    switch (object.direction)
    {
      case Direction::NW:
        if (0 == object.y % 2)
          return moveUp(object);
        else
          return moveUp(object) | moveLeft(object);
      case Direction::NE:
        if (0 == object.y % 2)
          return moveUp(object) | moveRight(object);
        else
          return moveUp(object);
      case Direction::E:
        return moveRight(object);
      case Direction::SE:
        if (0 == object.y % 2)
          return moveDown(object) | moveRight(object);
        else
          return moveDown(object);
      case Direction::SW:
        if (0 == object.y % 2)
          return moveDown(object);
        else
          return moveDown(object) | moveLeft(object);
      case Direction::W:
        return moveLeft(object);
      default:
        return false;
    }
  }

  const Dimensions mDimensions;
  Ship mShips[2];
  using controllerRef = ::std::reference_wrapper<Controller>;
  controllerRef mControllers[2];
  ::std::vector<Bullet> mBullets;

  ::std::ostream& mLogger;
};

#endif
