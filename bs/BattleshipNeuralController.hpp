//
// BattleshipNeuralController.hpp
//
//  Created on: 2019. m�rc. 13.
//      Author: fasz
//

#ifndef H690FF6F9_CA0B_4B67_89F6_F46AA3563494
#define H690FF6F9_CA0B_4B67_89F6_F46AA3563494

#include "Battleship.hpp"

class GenericRecurrentNetwork;

class BattleshipNeuralController : public Battleship::Controller
{
public:
  class InvalidNetworkException
  {
  };
  BattleshipNeuralController(GenericRecurrentNetwork& network, Battleship::Dimensions mapSize);

  virtual Command produceCommand(Battleship::Position myPosition,
    Battleship::Position enemyPosition,
    const ::std::vector<Battleship::Position>& bullets) override;

private:
  GenericRecurrentNetwork& mNetwork;
  Battleship::Dimensions mMapSize;

  ::std::vector<int> mInput;
};

#endif // H690FF6F9_CA0B_4B67_89F6_F46AA3563494
