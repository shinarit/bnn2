//
// Scenario.hpp
//
//  Created on: 2019. m�rc. 9.
//      Author: fasz
//

#ifndef H6733B553_532C_4E51_BF0C_395452BCB119
#define H6733B553_532C_4E51_BF0C_395452BCB119

#include <IScenario.hpp>

#include <iosfwd>

class BattleshipScenario : public IScenario
{
public:
  class InvalidSpecimenException
  {
  };

  struct Size
  {
    unsigned x, y;
  };
  struct Config
  {
    bool configure(::std::string settings);

    static const ::std::string HelpText;

    Size size;
    unsigned hiddenNodeCount;
    unsigned matchLength;
  };
  BattleshipScenario(Config config);

  virtual ::std::vector<IScenario::Score> scoreBreed(::std::vector<ISpecimen*> breed,
    ::std::mt19937& randomEngine,
    ::std::ostream& scoringLogger) const override;
  virtual ISpecimen* createSpecimen(::std::mt19937& randomEngine) const override;

private:
  static const IScenario::Score TIMEOUT_SCORE = 1;
  static const IScenario::Score DRAW_SCORE = 1;
  static const IScenario::Score WIN_SCORE = 3;
  static const IScenario::Score LOSE_SCORE = 0;

  const Config mConfig;
};

#endif // H6733B553_532C_4E51_BF0C_395452BCB119
