//
// BattleshipNeuralController.cpp
//
//  Created on: 2019. m�rc. 13.
//      Author: fasz
//

#include "BattleshipNeuralController.hpp"

#include <GenericRecurrentNetwork.hpp>

#include <cstring>

BattleshipNeuralController::BattleshipNeuralController(GenericRecurrentNetwork& network, Battleship::Dimensions mapSize)
  : mNetwork(network), mMapSize(mapSize), mInput(mMapSize.height * mMapSize.width)
{
  if (mMapSize.height * mMapSize.width != network.inputNumber() ||
    // turn and fire
    1 + 1 != network.outputNumber())
  {
    throw InvalidNetworkException();
  }
}

namespace
{
::std::optional<Battleship::Side> getSide(int input)
{
  if (input > 32)
    return Battleship::Side::LEFT;
  else if (input < -32)
    return Battleship::Side::RIGHT;
  return ::std::optional<Battleship::Side>();
}
} // namespace

Battleship::Controller::Command BattleshipNeuralController::produceCommand(Battleship::Position myPosition,
  Battleship::Position enemyPosition,
  const ::std::vector<Battleship::Position>& bullets)
{
  const int sideMarker(256);

  ::std::memset(mInput.data(), 0, sizeof(decltype(mInput[0])) * mInput.size());

  mInput[myPosition.y * mMapSize.width + myPosition.x] = sideMarker;
  mInput[enemyPosition.y * mMapSize.width + enemyPosition.x] = -sideMarker;
  for (auto& bullet : bullets)
  {
    mInput[bullet.y * mMapSize.width + bullet.x] = -sideMarker / 2;
  }

  mNetwork.step(mInput);

  return {getSide(mNetwork.getOutput(0)), getSide(mNetwork.getOutput(1))};
}
